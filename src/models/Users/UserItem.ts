export interface UserItem {
    id: string;
    username: string;
    avatar: string;
}
