import { UserItem } from '../Users/UserItem';

export interface MessageItem {
    id: string;
    text: string;
    author: UserItem;
    editedAt: Date | null;
    createdAt: Date;
    likers: UserItem[];
}
