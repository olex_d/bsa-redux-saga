export interface MessageResponseDto {
    id: string;
    text: string;
    user: string;
    avatar: string;
    userId: string;
    editedAt: Date | null;
    createdAt: Date;
}
