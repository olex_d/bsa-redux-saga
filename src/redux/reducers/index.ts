import { combineReducers } from 'redux';
import { chat } from '../../containers/Chat/reducer';

export default combineReducers({ chat });
