import { createRoutine } from 'redux-saga-routines';

export const fetchMessagesRoutine = createRoutine('GET_MESSAGES');
export const likeMessageRoutine = createRoutine('LIKE_MESSAGE');
export const createMessageRoutine = createRoutine('CREATE_MESSAGE');
export const toggleEditModalRoutine = createRoutine('TOGGLE_EDIT_MODAL');
export const editMessageRoutine = createRoutine('EDIT_MESSAGES');
export const deleteMessageRoutine = createRoutine('DELETE_MESSAGE');
export const processKeyUpEventRoutine = createRoutine('PROCESS_KEY_UP_MESSAGE');
