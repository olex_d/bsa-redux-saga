export const extractTimeFormDateTime = (date: Date): string => {
    return new Date(date).toLocaleTimeString([], {
        hour: '2-digit',
        minute: '2-digit',
    });
};

export const extractDateFromDatetime = (date: Date): string => {
    if (new Date().getDate() - new Date(date).getDate() === 1) {
        return 'Yesterday';
    } else if (new Date().getDate() - new Date(date).getDate() === 0) {
        return 'Today';
    }
    return new Date(date).toLocaleDateString('en-gb');
};

export const datetimeToOptionalDateAndTime = (date: Date): string => {
    let lastMessageTimeStr = '';
    if (new Date(date).getDate() !== new Date().getDate()) {
        lastMessageTimeStr += date.toLocaleDateString() + ' ';
    }
    lastMessageTimeStr += extractTimeFormDateTime(date);
    return lastMessageTimeStr;
};
