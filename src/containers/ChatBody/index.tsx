import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Comment as MessageUI, Divider, Grid, Header, Icon } from 'semantic-ui-react';
import style from './style.module.css';
import { MessageItem } from '../../models/Messages/MessageItem';
import EmptyChatBody from '../../components/EmptyChatBody';
import Message from '../../components/Message';
import { IBindingCallback1 } from '../../models/callback';
import { SemanticWIDTHS } from 'semantic-ui-react/dist/commonjs/generic';
import { Routine } from 'redux-saga-routines';
import { extractDateFromDatetime } from '../../utils/dateTimeFormatter';

interface ChatBodyProps {
    likeMessage: IBindingCallback1<Routine<any>>;
    toggleEditModal: IBindingCallback1<string>;
    deleteMessage: IBindingCallback1<string>;

    messages: MessageItem[];
}

let firstMessageDate = new Date().getDate();
const isSeparatorNeeded = (messageDate: Date): boolean => {
    const messageDateNum = new Date(messageDate).getDate();
    if (messageDateNum !== firstMessageDate) {
        firstMessageDate = messageDateNum;
        return true;
    }
    return false;
};

const ChatBody: React.FC<ChatBodyProps> = ({
    messages,
    likeMessage,
    toggleEditModal,
    deleteMessage,
}: ChatBodyProps) => {
    const [messagesCount, setMessagesCount] = useState(0);
    const [messagesCols, setMessagesCols] = useState<SemanticWIDTHS>(2);

    const messagesBottomRef = React.useRef<HTMLDivElement>(null);

    useLayoutEffect(() => {
        setMessagesCols(window.innerWidth < 768 ? 1 : 2);

        // appears already scrolled
        if (messagesBottomRef.current !== null) {
            messagesBottomRef.current.scrollIntoView();
        }
    }, []);

    useEffect(() => {
        if (messagesBottomRef.current !== null && messages.length !== messagesCount) {
            messagesBottomRef.current.scrollIntoView({ behavior: 'smooth' });
            setMessagesCount(messages.length);
        }
    }, [messages]);

    useEffect(() => {
        window.addEventListener('resize', () => {
            setMessagesCols(window.innerWidth < 768 ? 1 : 2);
        });
    }, [window]);

    return (
        <div className={style.chatBodyWrapper}>
            {messages?.length === 0 ? (
                <EmptyChatBody />
            ) : (
                <MessageUI.Group className={style.chatBody}>
                    <Grid columns={messagesCols} className={style.messagesGrid}>
                        <Grid.Row />
                        {messages
                            .sort((m1, m2) => new Date(m1.createdAt).getTime() - new Date(m2.createdAt).getTime())
                            .map((message) => (
                                <React.Fragment key={message.id}>
                                    {isSeparatorNeeded(message.createdAt) && (
                                        <Divider horizontal className={style.messagesDelimiter}>
                                            <Header as="h4">
                                                <Icon name="angle down" />
                                                {extractDateFromDatetime(message.createdAt)}
                                            </Header>
                                        </Divider>
                                    )}
                                    <Grid.Row>
                                        {message.author.id === process.env.REACT_APP_OWNER_USER_ID ? (
                                            <Grid.Column floated="right" textAlign="right">
                                                <Message
                                                    message={message}
                                                    likeMessage={likeMessage}
                                                    toggleEditModal={toggleEditModal}
                                                    deleteMessage={deleteMessage}
                                                />
                                            </Grid.Column>
                                        ) : (
                                            <Grid.Column floated="left">
                                                <Message
                                                    message={message}
                                                    likeMessage={likeMessage}
                                                    toggleEditModal={toggleEditModal}
                                                    deleteMessage={deleteMessage}
                                                />
                                            </Grid.Column>
                                        )}
                                    </Grid.Row>
                                </React.Fragment>
                            ))}
                        <div ref={messagesBottomRef} />
                    </Grid>
                </MessageUI.Group>
            )}
        </div>
    );
};

export default ChatBody;
