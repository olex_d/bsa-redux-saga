import React from 'react';
import { Container } from 'semantic-ui-react';
import Chat from '../Chat';
import PageHeader from '../../components/PageHeader';
import { store } from '../../redux/store';
import { Provider } from 'react-redux';
import KeyTriggerWrapper from '../../components/KeyTriggerWrapper';

const App: React.FC = () => (
    <Provider store={store}>
        <KeyTriggerWrapper>
            <Container>
                <PageHeader />
                <Chat />
            </Container>
        </KeyTriggerWrapper>
    </Provider>
);

export default App;
