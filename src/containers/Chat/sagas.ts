import { Routine } from 'redux-saga-routines';
import { all, call, put, takeEvery, select } from 'redux-saga/effects';
import * as messagesService from '../../services/messagesService';
import {
    createMessageRoutine,
    deleteMessageRoutine,
    editMessageRoutine,
    fetchMessagesRoutine,
    likeMessageRoutine,
} from '../../redux/routines';
import { MessageItem } from '../../models/Messages/MessageItem';
import { UserItem } from '../../models/Users/UserItem';
import { MessageResponseDto } from '../../models/Messages/MessageResponseDto';
import { generateUUID } from '../../utils/UUIDService';

const mapResponseToMessages = (responses: MessageResponseDto[]): MessageItem[] => {
    return responses.map((response) => {
        const author: UserItem = { id: response.userId, username: response.user, avatar: response.avatar };
        return {
            id: response.id,
            text: response.text,
            editedAt: response.editedAt,
            createdAt: response.createdAt,
            author,
            likers: [] as UserItem[],
        };
    });
};

const getUniqueUsers = (messages: MessageItem[]): UserItem[] => {
    return messages
        .filter((message, i, arr) => arr.findIndex((m) => m.author.id === message.author.id) === i)
        .map((message) => message.author);
};

export const getUserLastMessage = (messages: MessageItem[], userId: string): MessageItem => {
    const userMessages = messages
        .sort((m1, m2) => new Date(m1.createdAt).getTime() - new Date(m2.createdAt).getTime())
        .filter((message) => message.author.id === userId);
    return userMessages[userMessages.length - 1];
};

function* messagesFetch(): Routine<any> {
    try {
        const response = yield call(messagesService.fetchMessages);

        const messages = mapResponseToMessages(response);
        const users = getUniqueUsers(messages);

        yield put(fetchMessagesRoutine.success({ messages, users }));
    } catch (error) {
        yield put(fetchMessagesRoutine.failure(error));
    }
}

function* likeMessage({ payload }: Routine<any>): Routine<any> {
    try {
        const state = yield select();
        const clonedMessages = [...state.chat.messages];
        const users = state.chat.users;

        const likeMessageIndex = clonedMessages.findIndex((message) => message.id === payload.messageId);

        const newLiker = users.find((user: UserItem) => user.id === payload.userId);
        const likerIndex = clonedMessages[likeMessageIndex].likers.findIndex(
            (liker: UserItem) => liker.id === newLiker.id,
        );

        if (likerIndex === -1) {
            clonedMessages[likeMessageIndex].likers.push(newLiker);
        } else {
            clonedMessages[likeMessageIndex].likers.splice(likerIndex, 1);
        }

        yield put(likeMessageRoutine.success(clonedMessages));
    } catch (error) {
        yield put(likeMessageRoutine.failure(error));
    }
}

function* createMessage({ payload }: Routine<any>): Routine<any> {
    try {
        const state = yield select();
        const clonedMessages = [...state.chat.messages];
        const users = state.chat.users;

        const author = users.find((user: UserItem) => (user.id = payload.userId));
        const newMessage: MessageItem = {
            id: generateUUID(),
            text: payload.text,
            author,
            editedAt: null,
            createdAt: new Date(),
            likers: [] as UserItem[],
        };
        clonedMessages.push(newMessage);

        yield put(deleteMessageRoutine.success(clonedMessages));
    } catch (error) {
        yield put(deleteMessageRoutine.failure(error));
    }
}

function* editMessage({ payload }: Routine<any>): Routine<any> {
    try {
        const state = yield select();
        const clonedMessages = [...state.chat.messages];

        const editMessageIndex = clonedMessages.findIndex((message) => message.id === payload.messageId);
        clonedMessages[editMessageIndex].text = payload.text;

        yield put(editMessageRoutine.success(clonedMessages));
    } catch (error) {
        yield put(editMessageRoutine.failure(error));
    }
}

function* deleteMessage({ payload }: Routine<any>): Routine<any> {
    try {
        const state = yield select();
        const clonedMessages = [...state.chat.messages];
        const deleteMessageIndex = clonedMessages.findIndex((message) => message.id === payload);
        clonedMessages.splice(deleteMessageIndex, 1);

        yield put(deleteMessageRoutine.success(clonedMessages));
    } catch (error) {
        yield put(deleteMessageRoutine.failure(error));
    }
}

function* watchMessagesFetch(): any {
    yield takeEvery(fetchMessagesRoutine.TRIGGER, messagesFetch);
}

function* watchLikeMessage(): any {
    yield takeEvery(likeMessageRoutine.TRIGGER, likeMessage);
}

function* watchCreateMessage(): any {
    yield takeEvery(createMessageRoutine.TRIGGER, createMessage);
}

function* watchEditMessage(): any {
    yield takeEvery(editMessageRoutine.TRIGGER, editMessage);
}

function* watchDeleteMessage(): any {
    yield takeEvery(deleteMessageRoutine.TRIGGER, deleteMessage);
}

export default function* chatSagas(): any {
    yield all([
        watchMessagesFetch(),
        watchLikeMessage(),
        watchCreateMessage(),
        watchEditMessage(),
        watchDeleteMessage(),
    ]);
}
