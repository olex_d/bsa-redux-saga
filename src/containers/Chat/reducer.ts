import { MessageItem } from '../../models/Messages/MessageItem';
import { Routine } from 'redux-saga-routines';
import {
    createMessageRoutine,
    deleteMessageRoutine,
    editMessageRoutine,
    fetchMessagesRoutine,
    likeMessageRoutine,
    processKeyUpEventRoutine,
    toggleEditModalRoutine,
} from '../../redux/routines';
import { UserItem } from '../../models/Users/UserItem';
import { getUserLastMessage } from './sagas';

export interface ChatState {
    loading: boolean;
    messages: MessageItem[];
    users: UserItem[];
    editingMessage: MessageItem | undefined;
}

const initialState: ChatState = {
    loading: true,
    messages: [],
    users: [],
    editingMessage: undefined,
};

export const chat = (state = initialState, action: Routine<any>): ChatState => {
    switch (action.type) {
        case fetchMessagesRoutine.SUCCESS: {
            return {
                ...state,
                messages: action.payload.messages,
                users: action.payload.users,
                loading: false,
            };
        }
        case toggleEditModalRoutine.TRIGGER: {
            return {
                ...state,
                editingMessage:
                    state.editingMessage === undefined
                        ? state.messages.find((message) => message.id === action.payload)
                        : undefined,
            };
        }
        case processKeyUpEventRoutine.TRIGGER: {
            if (state.editingMessage === undefined) {
                return {
                    ...state,
                    editingMessage: getUserLastMessage(state.messages, action.payload),
                };
            } else {
                return state;
            }
        }
        case editMessageRoutine.SUCCESS: {
            return {
                ...state,
                editingMessage: undefined,
                messages: action.payload,
            };
        }
        case likeMessageRoutine.SUCCESS:
        case createMessageRoutine.SUCCESS:
        case deleteMessageRoutine.SUCCESS: {
            return {
                ...state,
                messages: action.payload,
            };
        }
        default:
            return state;
    }
};
