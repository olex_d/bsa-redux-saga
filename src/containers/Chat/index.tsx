import React, { useLayoutEffect } from 'react';
import { IBindingAction, IBindingCallback1 } from '../../models/callback';
import { MessageItem } from '../../models/Messages/MessageItem';
import Spinner from '../../components/Spinner';
import ChatHeader from '../../components/ChatHeader';
import ChatBody from '../ChatBody';
import { connect } from 'react-redux';
import {
    createMessageRoutine,
    deleteMessageRoutine,
    editMessageRoutine,
    fetchMessagesRoutine,
    likeMessageRoutine,
    toggleEditModalRoutine,
} from '../../redux/routines';
import { UserItem } from '../../models/Users/UserItem';
import { Routine } from 'redux-saga-routines';
import ChatInput from '../../components/ChatInput';
import EditModal from '../../components/EditModal';

interface ChatProps {
    fetchMessages: IBindingAction;
    likeMessage: IBindingCallback1<Routine<any>>;
    createMessage: IBindingCallback1<Routine<any>>;
    toggleEditModal: IBindingCallback1<string>;
    editMessage: IBindingCallback1<Routine<any>>;
    deleteMessage: IBindingCallback1<string>;

    loading: boolean;
    messages?: MessageItem[];
    users?: UserItem[];
    editingMessage: MessageItem;
}

const Chat: React.FunctionComponent<ChatProps> = ({
    fetchMessages,
    likeMessage,
    createMessage,
    toggleEditModal,
    editMessage,
    deleteMessage,
    loading,
    messages,
    users,
    editingMessage,
}: ChatProps) => {
    useLayoutEffect(() => {
        fetchMessages();
    }, []);

    return (
        <>
            {loading || messages === undefined || users === undefined ? (
                <Spinner />
            ) : (
                <div>
                    <ChatHeader chatName={'Lampoviy BSA Chatik'} users={users} messages={messages} />
                    <ChatBody
                        messages={messages}
                        likeMessage={likeMessage}
                        toggleEditModal={toggleEditModal}
                        deleteMessage={deleteMessage}
                    />
                    <ChatInput saveMessage={createMessage} />
                    {editingMessage !== undefined && (
                        <EditModal
                            editMessage={editMessage}
                            editingMessage={editingMessage}
                            toggleEditModal={toggleEditModal}
                        />
                    )}
                </div>
            )}
        </>
    );
};

const mapStateToProps = (state: any): any => {
    const {
        chat: { loading, messages, users, editingMessage },
    } = state;
    return {
        loading,
        messages,
        users,
        editingMessage,
    };
};

const mapDispatchToProps = {
    fetchMessages: fetchMessagesRoutine,
    likeMessage: likeMessageRoutine,
    createMessage: createMessageRoutine,
    toggleEditModal: toggleEditModalRoutine,
    editMessage: editMessageRoutine,
    deleteMessage: deleteMessageRoutine,
};

export default connect<ChatProps>(mapStateToProps, mapDispatchToProps)(Chat);
