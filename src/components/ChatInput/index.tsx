import React, { useState } from 'react';
import { Button, Form, Responsive, TextArea } from 'semantic-ui-react';
import style from './style.module.css';
import { IBindingCallback1 } from '../../models/callback';
import { Routine } from 'redux-saga-routines';
import { MessageItem } from '../../models/Messages/MessageItem';

interface ChatInputProps {
    saveMessage: IBindingCallback1<Routine<any>>;
    editingMessage?: MessageItem | undefined;
}

const ChatInput: React.FC<ChatInputProps> = ({ saveMessage, editingMessage }: ChatInputProps) => {
    const [text, setText] = useState(editingMessage === undefined ? '' : editingMessage.text);

    const clearText = (): void => {
        setText('');
    };

    const handleSaveMessage = (): void => {
        if (text === undefined || text.trim().length === 0) return;

        if (editingMessage !== undefined) {
            saveMessage({ messageId: editingMessage.id, text });
        } else {
            saveMessage({ userId: process.env.REACT_APP_OWNER_USER_ID, text });
        }
        clearText();
    };

    return (
        <div className={style.messageFormWrapper}>
            <Form onSubmit={handleSaveMessage} className={style.messageForm}>
                <div className={text.length > 0 ? style.messageFormGrid : 'hidden'}>
                    <TextArea
                        placeholder="Enter your message"
                        value={text}
                        onChange={(e: React.FormEvent<HTMLTextAreaElement>): void =>
                            setText((e.target as HTMLTextAreaElement).value)
                        }
                        className={style.messageTextArea}
                        rows={4}
                    />

                    {text.length > 0 && (
                        <>
                            <Responsive as={React.Fragment} minWidth={705}>
                                <Button
                                    content="Send"
                                    icon="arrow alternate circle up"
                                    labelPosition="left"
                                    className={style.sendButton}
                                    color="orange"
                                    onClick={(): void => handleSaveMessage()}
                                />
                            </Responsive>
                            <Responsive as={React.Fragment} maxWidth={704.9}>
                                <Button
                                    icon="arrow alternate circle up"
                                    className={style.sendButton}
                                    color="orange"
                                    onClick={(): void => handleSaveMessage()}
                                />
                            </Responsive>
                            <Responsive as={React.Fragment} minWidth={705}>
                                <Button
                                    content="Clear"
                                    icon="delete"
                                    labelPosition="left"
                                    className={style.clearButton}
                                    basic
                                    onClick={(): void => clearText()}
                                />
                            </Responsive>
                            <Responsive as={React.Fragment} maxWidth={704.9}>
                                <Button
                                    type="submit"
                                    icon="delete"
                                    className={style.clearButton}
                                    size="tiny"
                                    basic
                                    onClick={(): void => clearText()}
                                />
                            </Responsive>
                        </>
                    )}
                </div>
            </Form>
        </div>
    );
};

export default ChatInput;
