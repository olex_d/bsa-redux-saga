import React from 'react';
import { Grid, Icon, Popup } from 'semantic-ui-react';
import style from './style.module.css';

const PageHeader: React.FC = () => (
    <div className={style.pageHeader}>
        <Grid verticalAlign="middle" columns={2}>
            <Grid.Column floated="left">
                <img src="https://svgur.com/i/N4g.svg" alt="Logo" style={{ height: '55px' }} />
            </Grid.Column>
            <Grid.Column floated="right" textAlign="right">
                <Popup
                    content="Chat made by Alexander Danylchenko for BSA2020"
                    position="bottom right"
                    trigger={<Icon name="copyright" size="big" style={{ color: '#121212' }} />}
                    inverted
                    style={{ borderRadius: '20px' }}
                />
            </Grid.Column>
        </Grid>
    </div>
);

export default PageHeader;
