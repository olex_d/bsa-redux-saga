import React from 'react';
import { Icon, Label, Popup } from 'semantic-ui-react';
import { datetimeToOptionalDateAndTime } from '../../utils/dateTimeFormatter';
import style from './style.module.css';
import { MessageItem } from '../../models/Messages/MessageItem';
import { UserItem } from '../../models/Users/UserItem';

interface ChatStatisticProps {
    users: UserItem[];
    messages: MessageItem[];
}

// Return only time if day hasn't already passed, otherwise display full date
const getLastMessageTime = (messages: MessageItem[]): string => {
    const createdAtLs = messages.map((message) => new Date(message.createdAt).getTime());
    const lastMessageTime = new Date(Math.max(...createdAtLs));
    return datetimeToOptionalDateAndTime(lastMessageTime);
};

const ChatStatistic: React.FC<ChatStatisticProps> = ({ users, messages }: ChatStatisticProps) => (
    <div className={style.statistic}>
        <Popup
            size="tiny"
            hoverable
            content="Participants amount"
            position="bottom right"
            trigger={
                <Label className={style.statisticLabel}>
                    <Icon name="user" /> {users.length}
                </Label>
            }
            style={{ borderRadius: '20px' }}
        />

        <Popup
            size="tiny"
            hoverable
            content="Messages amount"
            position="bottom right"
            trigger={
                <Label className={style.statisticLabel}>
                    <Icon name="comments" /> {messages.length}
                </Label>
            }
            style={{ borderRadius: '20px' }}
        />

        <Popup
            size="tiny"
            hoverable
            content="Last message at"
            position="bottom right"
            trigger={
                <Label className={style.statisticLabel}>
                    <Icon name="hourglass zero" /> {getLastMessageTime(messages)}
                </Label>
            }
            style={{ borderRadius: '20px' }}
        />
    </div>
);

export default ChatStatistic;
