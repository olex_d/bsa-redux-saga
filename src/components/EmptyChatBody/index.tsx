import React from 'react';
import { Header, Icon } from 'semantic-ui-react';

const EmptyChatBody: React.FC = () => (
    <Header as="h2" icon textAlign="center">
        <Icon name="eye" />
        Not messages yet...
        <Header.Subheader>Be first who left the message.</Header.Subheader>
    </Header>
);

export default EmptyChatBody;
