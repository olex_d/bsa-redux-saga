import React from 'react';
import { MessageItem } from '../../models/Messages/MessageItem';
import { IBindingCallback1 } from '../../models/callback';
import { Routine } from 'redux-saga-routines';
import { Modal } from 'semantic-ui-react';
import ChatInput from '../ChatInput';
import style from './style.module.css';

interface EditModalProps {
    editingMessage: MessageItem;
    toggleEditModal: IBindingCallback1<string>;
    editMessage: IBindingCallback1<Routine<any>>;
}

const EditModal: React.FC<EditModalProps> = ({ editingMessage, editMessage, toggleEditModal }: EditModalProps) => (
    <Modal
        dimmer="blurring"
        open
        closeOnDimmerClick
        closeOnEscape
        closeIcon
        onClose={(): void => toggleEditModal(editingMessage.id)}
        className={style.editModal}
    >
        <Modal.Header>Edit your message</Modal.Header>
        <Modal.Content>
            <ChatInput saveMessage={editMessage} editingMessage={editingMessage} />
        </Modal.Content>
    </Modal>
);

export default EditModal;
