import React, { useState } from 'react';
import { Comment as MessageUI, Label, Popup } from 'semantic-ui-react';
import style from './style.module.css';
import { extractTimeFormDateTime } from '../../utils/dateTimeFormatter';
import { MessageItem } from '../../models/Messages/MessageItem';
import { IBindingCallback1 } from '../../models/callback';
import { Routine } from 'redux-saga-routines';

interface MessageProps {
    likeMessage: IBindingCallback1<Routine<any>>;
    toggleEditModal: IBindingCallback1<string>;
    deleteMessage: IBindingCallback1<string>;

    message: MessageItem;
}

const Message: React.FC<MessageProps> = ({ message, likeMessage, toggleEditModal, deleteMessage }: MessageProps) => {
    const [isToolbarOpen, setToolbarOpen] = useState(false);

    const messageItem = (
        <MessageUI
            className={message.author.id !== process.env.REACT_APP_OWNER_USER_ID ? style.message : style.ownMessage}
        >
            {message.author.id !== process.env.REACT_APP_OWNER_USER_ID && (
                <MessageUI.Avatar src={message.author.avatar} />
            )}
            <MessageUI.Content>
                {message.author.id !== process.env.REACT_APP_OWNER_USER_ID && (
                    <MessageUI.Author as="a" content={message.author.username} />
                )}
                <MessageUI.Metadata>
                    {extractTimeFormDateTime(message.createdAt)}{' '}
                    {message.editedAt !== null && new Date(message.editedAt).toString() !== 'Invalid Date' && (
                        <span>(edited)</span>
                    )}
                </MessageUI.Metadata>

                <MessageUI.Text content={message.text} />
                <MessageUI.Actions>
                    <>
                        <Label
                            basic
                            size="small"
                            as="a"
                            className={`${style.toolbarBtn} ${
                                message.author.id === process.env.REACT_APP_OWNER_USER_ID
                                    ? style.disabledButton
                                    : style.likeButton
                            }`}
                            icon="thumbs up"
                            content={message.likers.length}
                            onClick={(): void =>
                                likeMessage({ userId: process.env.REACT_APP_OWNER_USER_ID, messageId: message.id })
                            }
                        />
                    </>
                </MessageUI.Actions>
            </MessageUI.Content>
        </MessageUI>
    );

    return (
        <Popup
            trigger={messageItem}
            disabled={message.author.id !== process.env.REACT_APP_OWNER_USER_ID}
            open={isToolbarOpen}
            on="hover"
            onOpen={(): void => setToolbarOpen(true)}
            onClose={(): void => setToolbarOpen(false)}
            hoverable
            content={
                <div className={style.ownMessageActions}>
                    <Label
                        basic
                        size="huge"
                        as="a"
                        className={`${style.toolbarBtn} ${style.editButton}`}
                        icon="cog"
                        onClick={(): void => {
                            setToolbarOpen(false);
                            toggleEditModal(message.id);
                        }}
                    />
                    <Label
                        basic
                        size="huge"
                        as="a"
                        className={`${style.toolbarBtn} ${style.deleteButton}`}
                        icon="trash"
                        onClick={(): void => deleteMessage(message.id)}
                    />
                </div>
            }
        />
    );
};

export default Message;
