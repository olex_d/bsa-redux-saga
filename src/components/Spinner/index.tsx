import React from 'react';
import { Dimmer, Header, Loader } from 'semantic-ui-react';

const Spinner: React.FC = () => (
    <Dimmer active inverted>
        <Loader size="massive" inverted>
            <Header as="h1">Loading BSA Chat</Header>
            <Header as="h6">(Designers were harmed during the creation)</Header>
        </Loader>
    </Dimmer>
);

export default Spinner;
