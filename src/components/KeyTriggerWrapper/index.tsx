import React from 'react';
import { processKeyUpEventRoutine } from '../../redux/routines';
import { connect } from 'react-redux';
import { IBindingCallback1 } from '../../models/callback';
import style from './style.module.css';

interface KeyTriggerWrapperProps {
    children: any;
    processKeyUpEvent: IBindingCallback1<string>;
}

const KeyTriggerWrapper: React.FC<KeyTriggerWrapperProps> = ({
    children,
    processKeyUpEvent,
}: KeyTriggerWrapperProps) => {
    const handleKeyUpClick = (e: KeyboardEvent): void => {
        if (e.key === 'ArrowUp' && processKeyUpEvent !== undefined) {
            processKeyUpEvent(process.env.REACT_APP_OWNER_USER_ID);
        }
    };

    return (
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        <div tabIndex={0} onKeyUp={(e: KeyboardEvent): void => handleKeyUpClick(e)} className={style.keyTrigger}>
            {children}
        </div>
    );
};

// to allow usage without passing mandatory props
const mapStateToProps = (): any => {
    return {};
};

const mapDispatchToProps = {
    processKeyUpEvent: processKeyUpEventRoutine,
};

export default connect<KeyTriggerWrapperProps>(mapStateToProps, mapDispatchToProps)(KeyTriggerWrapper);
