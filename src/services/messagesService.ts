import { MessageItem } from '../models/Messages/MessageItem';

export const fetchMessages = async (): Promise<MessageItem> => {
    const response = await fetch(process.env.REACT_APP_MESSAGES_LIST_API);
    return await response.json();
};
